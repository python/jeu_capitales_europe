# -*- coding: utf-8 -*-

from random import sample

d={
    'France' : 'Paris',
    'Espagne' : 'Madrid',
    'Royaume-Uni' : 'Londres',
    'Allemagne' : 'Berlin',
    'Suisse' : 'Berne',
    'Italie' : 'Rome',
    'Autriche' : 'Viennes',
    'Hongrie' : 'Budapest',
    'Irlande' : 'Dublin',
    'Islande' : 'Reykjavik',
    'Danemark' : 'Copenhague',
    'Roumanie' : 'Bucarest',
    'Turquie' : 'Ankara',
    'Russie' : 'Moscou',
    'Grèce' : 'Athènes',
    'Pologne' : 'Varsovie',
    'Croatie' : 'Zagreb',
    'Tchèquie' : 'Prague',
    'Ukraine' : 'Kiev',
    'Belgique' : 'Bruxelles',
    'Luxembourg' : 'Luxembourg',
    'Andorre' : 'Andorre-la-Vieille',
    'Monaco' : 'Monaco',
    'Vatican' : 'Vatican',
    'Pays-Bas' : 'Amsterdam',
    'Portugal' : 'Lisbonne',
    'Malte' : 'La Valette',
    'Liechtenstein' : 'Vaduz',
    'Slovaquie' : 'Bratislava',
    'Slovénie' : 'Ljubljana',
    'Bosnie-Herzégovine' : 'Sarajevo',
    'Monténégro' : 'Podgorica',
    'Albanie' : 'Tirana',
    'Serbie' : 'Belgrade',
    'Macédoine' : 'Skopje',
    'Bulgarie' : 'Sofia',
    'Moldavie' : 'Chisinau',
    'Biélorussie' : 'Minsk',
    'Lituanie' : 'Vilnius',
    'Lettonie' : 'Riga',
    'Estonie' : 'Tallinn',
    'Finlande' : 'Helsinki',
    'Norvège' : 'Oslo',
    'Suède' : 'Stockholm',
    'Chypre' : 'Nicosie'
    }

def verif() :
    verif=input("Voulez-vous continuer ? (o/n) ").lower()
    if verif=='n' : return False
    elif verif=='' : exit()
    else : return True

nt=5
still=True
moy=[0,0]
while still :
    n=0
    for quest in sample(d.keys(),nt) :
        ans=input("Quelle est la capitale de ce pays : "+quest+" ?\n")
        if d[quest].lower()==ans.lower() :
                print("Bonne réponse !\n")
                n+=1
        else : print("Mauvaise réponse ! Il fallait répondre :",d[quest],"\n")
    print("C'est terminé ! << Score : ",n,"/",nt," >>\n")
    moy[0]+=n
    moy[1]+=nt
    still=verif()
    print("------------------------------\n")

print("Votre moyenne est de",round(nt*moy[0]/moy[1],2),"/",nt)
